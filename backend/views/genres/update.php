<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Genres */

$this->title = 'Редактировать жанр: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Жанры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->genre_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="genres-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
