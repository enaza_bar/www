<?php



use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use common\models\Statuses;
use common\models\Genres;

/* @var $model common\models\Visitors */


$statuses = Statuses::find()->orderBy('statuses.status_id ASC')->all();
//$genres = Genres::find()->orderBy('genres.genre_id ASC')->all();
?>

<div class="visitors-form">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'status_id')->dropDownList(
        ArrayHelper::map($statuses, 'status_id', 'status'),
        ['prompt' => '--------'])
    ?>
    <?$model->genre_id = $model->genres?>
    <?= $form->field($model, 'genre_id')
        ->listBox(ArrayHelper::map(Genres::find()->all(), 'genre_id', 'name'),
            ['multiple' => true]
        ) ?>

    <div class="form-group">
		<?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить',
			['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
