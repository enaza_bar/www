<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Visitors */

$this->title = 'Редактировать данные посетителя' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Посетители', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->visitor_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="visitors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
