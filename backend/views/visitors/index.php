<?php

use yii\grid\GridView;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use common\models\Section;
use common\models\Statuses;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\Visitors */

$this->title = 'Посетители';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="visitors-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Пригласить посетителя', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'visitor_id',
            'name',
            [
                'attribute' => 'status_id',
                'value' => function ($data) {
                    return $data->status->status;
                },
                'filter' => ArrayHelper::map(
                    Statuses::find()->where(['is not', 'status_id', null])->asArray()->all(),
                    'status_id',
                    'status'
                ),
            ],
			[
				'attribute' => 'genres_id',
				'value' => function ($data) {
                    $html = "";
					foreach ($data->genres as $gr){
					    $html .= "{$gr->name} ";
                    }
					return $html;
				},
			],


            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
