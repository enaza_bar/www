<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Statuses */

$this->title = 'Редактировать статус: ' . $model->status_id;
$this->params['breadcrumbs'][] = ['label' => 'Статусы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->status_id, 'url' => ['view', 'id' => $model->status_id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="statuses-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
