<?php

namespace backend\components\rbac;

use Yii;
use yii\rbac\Permission;
use yii\rbac\Role;
use yii\rbac\Rule;

/**
 * Проверяем user на соответствие с пользователем, переданным через параметры
 */
class AdminRule extends Rule
{
    public $name = 'isAdmin';
    /**
     * @param string|int $user the user ID.
     * @param Role|Permission $item the role or permission that this rule is associated width.
     * @param array $params parameters passed to ManagerInterface::checkAccess().
     * @return bool a value indicating whether the rule permits the role or permission it is associated with.
     */
    public function execute($user, $item, $params)
    {
        if (Yii::$app->user->can('admin')) {
            return true;
        }

        return false;
    }
}
