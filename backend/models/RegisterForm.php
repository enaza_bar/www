<?php

namespace backend\models;

use common\models\Section;
use Yii;
use yii\base\Model;

/**
 * Форма для пользователей
 */
class RegisterForm extends Model
{
    public $id;
    public $username;
    public $email;
    public $password;
    public $sections = [];

    /**
     * @var User
     */
    public $user;

    /**
     * RegisterForm constructor.
     *
     * @param array $config
     * @param User $user
     */
    public function __construct(array $config = [], $user = null)
    {
        if ($user === null) {
            $this->user = new User();
        } else {
            $this->user = $user;
        }
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        $createConf = [];

        if ($this->user->isNewRecord) {
            $createConf = [
                [
                    'username',
                    'unique',
                    'targetClass' => '\backend\models\User',
                    'message' => 'Пользоватлеь с таким логином уже существует.'
                ],
                [
                    'email',
                    'unique',
                    'targetClass' => '\backend\models\User',
                    'message' => 'Пользователь с таким адресом электронной почты уже существует.'
                ],
            ];
        }

        return [
                ['username', 'trim'],
                ['username', 'required'],
                ['username', 'string', 'min' => 2, 'max' => 255],

                ['email', 'trim'],
                ['email', 'required'],
                ['email', 'email'],
                ['email', 'string', 'max' => 255],

                ['password', 'string', 'min' => 6, 'skipOnEmpty' => !$this->user->isNewRecord],
            ] + $createConf;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ИД',
            'username' => 'Имя пользователя',
            'email' => 'Адрес электронной почты',
            'password' => 'Пароль',
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        if (!empty($data['User']) && !empty($data['User']['section'])) {
            foreach ($data['User']['section'] as $section) {
                $this->sections[] = Section::findOne(['id' => $section]);
            }
        }

        return parent::load($data, $formName);
    }

    /**
     * Сохранение нового пользователя.
     *
     * @return User|null the saved model or null if saving fails
     * @throws \yii\base\Exception
     * @throws \Exception
     */
    public function save()
    {
        if (!$this->validate()) {
            return null;
        }

        $isNewUser = $this->user->isNewRecord;

        $this->user->username = $this->username;
        $this->user->email = $this->email;

        if (!$isNewUser) {
            $this->user->id = $this->id;
        }

        if ($this->password) {
            $this->user->setPassword($this->password);
        }
        $this->user->generateAuthKey();


        $this->user->save();

        if (!empty($this->sections)) {
            $this->user->unlinkAll('section', true);

            foreach ($this->sections as $section) {
                $this->user->link('section', $section);
            }
        }

        if ($isNewUser) {
            $auth = Yii::$app->authManager;
            $authorRole = $auth->getRole('author');
            $auth->assign($authorRole, $this->user->getId());
        }

        return $this->user;
    }
}
