<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "visitors".
 *
 * @property int $visitor_id
 * @property string $name
 * @property int $status_id
 *
 * @property Statuses $status
 */
class Visitors extends ActiveRecord
{
    /**
     * @var Visitors
     */
    public $visitor;

    public $genre_id;


    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'visitors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'status_id', 'genre_id'], 'required'],
            [['status_id'], 'default', 'value' => null],
            [['status_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Statuses::className(), 'targetAttribute' => ['status_id' => 'status_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'visitor_id' => 'ИД',
            'name' => 'ИМЯ',
            'status_id' => 'Статус',
			'genres_id' => 'Жанры',
        ];
    }


    public function save($runValidation = false, $attributeNames = null)
    {

        if (!parent::save($runValidation)) {
            return false;
        }

		$this->unlinkAll('genres', true);
        foreach ($this->genre_id as $item){
			$this->link('genres', $this, ['genres_id' => intval($item)]);
		}

        return true;
    }


	/**
	 * @return \yii\db\ActiveQuery
	 * @throws \yii\base\InvalidConfigException
	 */
	public function getGenres()
	{
		return $this->hasMany(Genres::className(), ['genre_id' => 'genres_id'])
			->viaTable('genres_visitors', ['visitors_id' => 'visitor_id']);

	}

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Statuses::className(), ['status_id' => 'status_id']);
    }


}
