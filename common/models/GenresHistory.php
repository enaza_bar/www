<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "genres_history".
 *
 * @property int $genres_history_id
 * @property int $genre_id
 *
 * @property Genres $genre
 */
class GenresHistory extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return 'genres_history';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules()
	{
		return [
			[['genre_id'], 'required'],
			[['genre_id'], 'default', 'value' => null],
			[['genre_id'], 'integer'],
			[['genre_id'], 'exist', 'skipOnError' => true, 'targetClass' => Genres::className(), 'targetAttribute' => ['genre_id' => 'genre_id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels()
	{
		return [
			'genres_history_id' => 'Genres History ID',
			'genre_id' => 'Genre ID',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGenre()
	{
		return $this->hasOne(Genres::className(), ['genre_id' => 'genre_id']);
	}
}
