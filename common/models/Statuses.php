<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "statuses".
 *
 * @property int $status_id
 * @property string $status
 */
class Statuses extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'statuses';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'required'],
            [['status'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status_id' => 'ИД',
            'status' => 'Статус',
        ];
    }
}
