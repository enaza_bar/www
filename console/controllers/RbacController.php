<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;
use \backend\components\rbac\AdminRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $rule = new AdminRule();
        $auth->add($rule);

        $admin = $auth->createRole('admin');
        $auth->add($admin);

        $auth->assign($admin, 1);
    }
}
