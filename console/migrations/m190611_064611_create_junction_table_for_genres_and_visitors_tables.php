<?php

use yii\db\Migration;

/**
 * Handles the creation of table `genres_visitors`.
 * Has foreign keys to the tables:
 *
 * - `genres`
 * - `visitors`
 */
class m190611_064611_create_junction_table_for_genres_and_visitors_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('genres_visitors', [
            'genres_id' => $this->integer(),
            'visitors_id' => $this->integer(),
            'PRIMARY KEY(genres_id, visitors_id)',
        ]);

        // creates index for column `genres_id`
        $this->createIndex(
            'idx-genres_visitors-genres_id',
            'genres_visitors',
            'genres_id'
        );

        // add foreign key for table `genres`
        $this->addForeignKey(
            'fk-genres_visitors-genres_id',
            'genres_visitors',
            'genres_id',
            'genres',
            'genre_id',
            'CASCADE'
        );

        // creates index for column `visitors_id`
        $this->createIndex(
            'idx-genres_visitors-visitors_id',
            'genres_visitors',
            'visitors_id'
        );

        // add foreign key for table `visitors`
        $this->addForeignKey(
            'fk-genres_visitors-visitors_id',
            'genres_visitors',
            'visitors_id',
            'visitors',
            'visitor_id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `genres`
        $this->dropForeignKey(
            'fk-genres_visitors-genres_id',
            'genres_visitors'
        );

        // drops index for column `genres_id`
        $this->dropIndex(
            'idx-genres_visitors-genres_id',
            'genres_visitors'
        );

        // drops foreign key for table `visitors`
        $this->dropForeignKey(
            'fk-genres_visitors-visitors_id',
            'genres_visitors'
        );

        // drops index for column `visitors_id`
        $this->dropIndex(
            'idx-genres_visitors-visitors_id',
            'genres_visitors'
        );

        $this->dropTable('genres_visitors');
    }
}
