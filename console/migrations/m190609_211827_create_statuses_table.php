<?php

use yii\db\Migration;

/**
 * Handles the creation of table `statuses`.
 */
class m190609_211827_create_statuses_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('statuses', [
            'status_id' => $this->primaryKey(),
            'status' => $this->string(255)->notNull(),
        ]);

        // creates index for column `status_id`
        $this->createIndex(
            'idx-statuses-status_id',
            'statuses',
            'status_id'
        );

        $this->batchInsert('statuses',['status'],[
            ['Идет выпивать'],
            ['Идет танцевать'],
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-statuses-status_id',
            'statuses'
        );

        $this->dropTable('statuses');
    }
}
