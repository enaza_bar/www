<?php

use yii\db\Migration;

/**
 * Handles the creation of table `genres`.
 */
class m190609_223420_create_genres_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('genres', [
            'genre_id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->createIndex(
            'idx-genres-genre_id',
            'genres',
            'genre_id'
        );

        $this->batchInsert('genres',['name'],[
            ['Поп'],
            ['Инди'],
            ['Рок'],
            ['Альтернатива'],
            ['Электроника'],
            ['Джаз'],
        ]);
    }



    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-genres-genre_id',
            'genres'
        );

        $this->dropTable('genres');
    }
}
