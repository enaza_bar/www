<?php

use yii\db\Migration;

/**
 * Class m190612_110644_add_user
 */
class m190612_110644_add_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%user}}', [
            'id' => 1,
            'username' => 'admin',
            'auth_key' => 'KPnFpBHhf3FSByjxLIF42QFSOp88-2h3',
            'password_hash' => '$2y$13$MfzqRmP2W41S88eA.KvtDeoQ/HeeY4U6.5K4nFVBuosR8ufMGowOS',
            'password_reset_token' => null,
            'email' => 'ra@ro.ru',
            'status' => 10,
            'created_at' => 1560108924,
            'updated_at' => 1560108924,
            'verification_token' => 'TK_fB9z0mxkRZ-XWjwpp9aS79MgZOkuI_1559325738',
        ]);

        $this->insert('{{%auth_item}}', [
            'name' => 'admin',
            'type' => 1,
            'description' => null,
            'rule_name' => null,
            'data' => null,
            'created_at' => 1560108924,
            'updated_at' => 1560108924,
        ]);

        $this->insert('{{%auth_assignment}}', [
            'item_name' => 'admin',
            'user_id' => 1,
            'created_at' => 1560108924,
        ]);


        $this->insert('{{%auth_rule}}', [
            'name' => 'isAdmin',
            'data' => 'O:33:"backend\components\rbac\AdminRule":3:{s:4:"name";s:7:"isAdmin";s:9:"createdAt";i:1560108924;s:9:"updatedAt";i:1560108924;}',
            'created_at' => 1560108924,
            'updated_at' => 1560108924,
        ]);


        $this->batchInsert('genres_history', ['genre_id',], [
            [1],
            [2],
            [3],
        ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m190612_110644_add_user cannot be reverted.\n";

        return false;
    }
}
