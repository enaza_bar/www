<?php

use yii\db\Migration;

/**
 * Handles the creation of table `visitors`.
 */
class m190609_231611_create_visitors_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('visitors', [
            'visitor_id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'status_id' =>$this->integer()->notNull(),
        ]);

        // creates index for column `visitor_id`
        $this->createIndex(
            'idx-visitors-visitor_id',
            'visitors',
            'visitor_id'
        );

        // add foreign key for table `visitors`
        $this->addForeignKey(
            'fk-visitors-status_id',
            'visitors',
            'status_id',
            'statuses',
            'status_id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `visitors`
        $this->dropForeignKey(
            'fk-visitors-status_id',
            'visitors'
        );

        // drops index for column `visitor_id`
        $this->dropIndex(
            'idx-visitors-visitor_id',
            'visitors'
        );
        $this->dropTable('visitors');
    }
}
