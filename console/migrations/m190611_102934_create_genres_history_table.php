<?php

use yii\db\Migration;

/**
 * Handles the creation of table `genres_history`.
 */
class m190611_102934_create_genres_history_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('genres_history', [
            'genres_history_id' => $this->primaryKey(),
			'genre_id' =>$this->integer()->notNull(),
        ]);

		// creates index for column `genre_id`
		$this->createIndex(
			'idx-genres_history-genre_id',
			'genres_history',
			'genre_id'
		);

		// creates index for column `genres_history_id`
		$this->createIndex(
			'idx-genres_history-genres_history_id',
			'genres_history',
			'genres_history_id'
		);

		// add foreign key for table `genres_history`
		$this->addForeignKey(
			'fk-genres_history-genre_id',
			'genres_history',
			'genre_id',
			'genres',
			'genre_id',
			'CASCADE'
		);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
		// drops foreign key for table `visitors`
		$this->dropForeignKey(
			'fk-genres_history-genre_id',
			'genres_history'
		);

		// drops index for column `genre_id`
		$this->dropIndex(
			'idx-genres_history-genre_id',
			'genres_history'
		);

		$this->dropIndex(
			'idx-genres_history-genres_history_id',
			'genres_history'
		);

        $this->dropTable('genres_history');
    }
}
