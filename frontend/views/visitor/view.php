<?php


use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\DetailView;

use common\models\Statuses;

/* @var $this yii\web\View */
/* @var $model common\models\Visitors */

$this->title = $model->name;
?>
<div class="visitors-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Выгнать', ['kick', 'id' => $model->visitor_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы действительно хотите выгнать посетителя?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('К списку', ['/'], ['class' => 'btn btn-primary']) ?>
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'visitor_id',
            'name',
            [
                'attribute' => 'status_id',
                'value' => function ($data) {
                    return $data->status->status;
                },
                'filter' => ArrayHelper::map(
                    Statuses::find()->where(['is not', 'status_id', null])->asArray()->all(),
                    'status_id',
                    'status'
                ),
            ],
            [
                'attribute' => 'genres_id',
                'value' => function ($data) {
                    $html = "";
                    foreach ($data->genres as $gr){
                        $html .= "{$gr->name} ";
                    }
                    return $html;
                },
            ],
        ],
    ]) ?>
</div>
