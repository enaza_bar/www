<?php

use yii\grid\GridView;

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use common\models\Section;
use common\models\Statuses;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $model common\models\Visitors */

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Сейчас играет <?= $currentMusic ?>!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><?= Html::a('Сменить трек', ['change-music'], ['class' => 'btn btn-lg btn-success']) ?></p>
        <!--        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>-->
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-xs-12">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'name',
                        [
                            'attribute' => 'status_id',
                            'value' => function ($data) {
                                return $data->status->status;
                            },
                            'filter' => ArrayHelper::map(
                                Statuses::find()->where(['is not', 'status_id', null])->asArray()->all(),
                                'status_id',
                                'status'
                            ),
                        ],

                        [
                            'attribute' => 'genres_id',
                            'value' => function ($data) {
                                $html = "";
                                foreach ($data->genres as $gr) {
                                    $html .= "{$gr->name} ";
                                }
                                return $html;
                            },
                        ],


                        [
                            'class' => 'yii\grid\ActionColumn',
                            'template' => '{view} {delete}',
                            'buttons' => [
                                'delete' => function ($myUrl, $model, $key) {
                                    return Html::a('', ['/visitor/delete/' . $model->visitor_id], ['class' => 'glyphicon glyphicon-trash']);
                                },
                                'view' => function ($myUrl, $model, $key) {
                                    return Html::a('', ['/visitor/view/' . $model->visitor_id], ['class' => 'glyphicon glyphicon-eye-open']);
                                },
                            ],


                        ],
                    ],
                ]); ?>
            </div>
            <p style="text-align: right;">
                <?= Html::a('Кто то пришел', ['visitor/create'], ['class' => 'btn btn-lg btn-success']) ?>
                <?=($exitsVisitors) ? Html::a('Кто то ушел', ['visitor/rand-delete'], ['class' => 'btn btn-lg btn-danger']):''?>
                <?= Html::a('Проверить бар', ['check-bar-status'], ['class' => 'btn btn-lg btn-warning']) ?>
            </p>
        </div>

    </div>
</div>
