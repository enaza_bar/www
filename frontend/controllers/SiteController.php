<?php

namespace frontend\controllers;

use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

use common\models\Genres;
use common\models\Visitors;
use common\models\GenresHistory;
use yii\web\Response;


/**
 * Site controller
 */
class SiteController extends Controller
{

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	/**
	 * Index page
	 *
	 * @return mixed
	 * @throws NotFoundHttpException
	 */
	public function actionIndex()
	{

		$dataProvider = new ActiveDataProvider([
			'query' => Visitors::find(),
		]);

		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'currentMusic' => $this->getCurrentMusic(),
			'exitsVisitors' => $this->checkExistVisitors(),
		]);
	}

	/**
	 * Change current music
	 * The browser will be redirected to the 'index' page.
	 *
	 * @return Response
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 */
	public function actionChangeMusic()
	{

		$lastGenre = $this->getLastMusic();
		$lastPlayMusic = $lastGenre->genre->genre_id;

		$variants = Genres::find()->select(['genre_id'])->where(['!=', 'genre_id', $lastPlayMusic])->asArray()->all();

		$newGenreHistory = new GenresHistory();
		$newGenreHistory->genre_id = $variants[array_rand($variants)]['genre_id'];
		$newGenreHistory->save();

		$this->changeStatus();

		return $this->redirect(['index']);
	}

	/**
	 * Check the status bar
	 * The browser will be redirected to the 'index' page.
	 *
	 * @return Response
	 * @throws \Throwable
	 */
	public function actionCheckBarStatus()
	{
		$this->changeStatus();

		return $this->redirect(['index']);
	}

	/**
	 * The change of status of visitors
	 *
	 * @throws \Throwable
	 */
	protected function changeStatus()
	{
		$visitors = $this->getVisitors();

		foreach ($visitors as $visitor) {
			$genres = [];
			foreach ($visitor->genres as $genre) {
				$genres[] = $genre->name;
			}

			$visitor->status_id = (in_array($this->getCurrentMusic(), $genres)) ? 2 : 1;
			$visitor->update(true, ['status_id']);
		}
	}

	/**
	 * Gets all visitors
	 *
	 * @return Visitors visitors
	 * @throws NotFoundHttpException
	 */
	protected function getVisitors()
	{
		if (($visitors = Visitors::find()->all()) !== null) {
			return $visitors;
		}

		throw new NotFoundHttpException('The visitors does not exist.');
	}

	/**
	 * Gets current music
	 *
	 * @return mixed
	 * @throws NotFoundHttpException
	 */
	protected function getCurrentMusic()
	{
		$genresHistory = $this->getLastMusic();
		return $genresHistory->genre->name;
	}

	/**
	 * Gets last music
	 *
	 * @return last music
	 * @throws NotFoundHttpException
	 */
	protected function getLastMusic()
	{
		if (($lastMusic = GenresHistory::find()->orderBy(['genres_history_id' => SORT_DESC])->one()) !== null) {
			return $lastMusic;
		}

		throw new NotFoundHttpException('The requested music does not exist.');
	}

	/**
	 * Checking exist visitors in bar
	 *
	 * @return bool
	 */
	protected function checkExistVisitors()
	{
		if (!empty(Visitors::find()->asArray()->all())) {
			return true;
		}

		return false;
	}


}
