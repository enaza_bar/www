<?php

namespace frontend\controllers;

use yii\web\Controller;
use yii\web\NotFoundHttpException;

use common\models\Genres;
use common\models\Visitors;
use common\models\Statuses;

/**
 * Visitor controller
 */
class VisitorController extends Controller
{

	/**
	 * {@inheritdoc}
	 */
	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	/**
	 * Deletes an existing Visitors model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param integer $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public function actionDelete($id)
	{
		$this->findModel($id)->delete();

		return $this->redirect(['site/index']);
	}

	/**
	 * View visitor
	 *
	 * @param $id
	 *
	 * @return string
	 */
	public function actionView($id)
	{
		return $this->render('view', [
			'model' => Visitors::findOne($id),
		]);
	}

	/**
	 * Creates a new random Visitors model.
	 *
	 * @return mixed
	 * @throws NotFoundHttpException
	 */

	public function actionCreate()
	{
		$visitor = new Visitors();

		$names = ['Игорь', 'Костя', 'Евгений', 'Федор', 'Олег'];
		$randName = $names[rand(0, count($names)-1)];
		$visitor->name = $randName;

		$statuses = $this->getStatuses();
		$randStatus = $statuses[array_rand($statuses)]['status_id'];
		$visitor->status_id = $randStatus;

		$allGenres = $this->getGenres();
		$randGenres = $this->getRandomGenres($allGenres);
		$visitor->genre_id = $randGenres;

		$visitor->save();

		return $this->redirect(['site/index']);
	}

	/**
	 * Delete random visitor model
	 *
	 * @throws \Throwable
	 * @throws \yii\db\StaleObjectException
	 */
	public
	function actionRandDelete()
	{
		$visitors = $this->getVisitors();

		$this->actionDelete($visitors[array_rand($visitors)]['visitor_id']);
	}

	/**
	 * Finds the Visitors model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param integer $id
	 *
	 * @return Visitors the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id)
	{
		if (($model = Visitors::findOne($id)) !== null) {
			return $model;
		}

		throw new NotFoundHttpException('The requested visitor does not exist.');
	}

	/**
	 * Finds the Visitors model
	 *
	 * @return Visitors the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function getVisitors()
	{
		if (!empty(($visitors = Visitors::find()->asArray()->all()))) {
			return $visitors;
		}

		throw new NotFoundHttpException('Visitors is not exist');
	}

	/**
	 * Finds the Genres model
	 *
	 * @return Genres the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function getGenres()
	{
		if (!empty(($genres = Genres::find()->asArray()->all()))) {
			return $genres;
		}

		throw new NotFoundHttpException('Genres is not exist');
	}

	/**
	 * Gets random count random genres
	 *
	 * @param array $genres
	 *
	 * @return array random genres
	 */
	protected function getRandomGenres($genres)
	{
		$randGenresCount = rand(1, count($genres));
		$randGenres = (array)array_rand($genres, $randGenresCount);

		$result = [];
		for ($i = 0; $i < count($randGenres); $i++) {
			array_push($result, $genres[$randGenres[$i]]['genre_id']);
		}

		return $result;
	}

	/**
	 * Finds the Statuses model
	 *
	 * @return Statuses the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function getStatuses()
	{
		if (!empty(($statuses = Statuses::find()->asArray()->all()))) {
			return $statuses;
		}

		throw new NotFoundHttpException('Statuses is not exist');
	}


}